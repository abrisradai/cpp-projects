#version 130

in vec3 vs_out_col;
in vec3 vs_out_pos;

uniform int col_switch;
uniform float t;

out vec4 fs_out_col;

void main()
{
	float alpha = 0.5 * sin(2 * 3.14159 * t/5) + 0.5;
	switch(col_switch){
		case 1:
			fs_out_col = (1 - alpha) * vec4(1, 0, 0, 1) + alpha * vec4(vs_out_col, 1);
			break;
		case 2:
			fs_out_col = (1 - alpha) * vec4(0, 1, 0, 1) + alpha * vec4(vs_out_col, 1);
			break;
		case 3:
			fs_out_col = (1 - alpha) * vec4(0, 0, 1, 1) + alpha * vec4(vs_out_col, 1);
			break;
		default:
			fs_out_col = vec4(vs_out_col, 1);
			break;
	}
}